<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css" >
    <title>Form</title>
</head>
<body>
	<?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}


    ?>
	<div class="form">
		<form action="" method="post">
			<label>
		        Enter your name:<br />
		        <input type="text" name="fieldname" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio'];?> " />
      		</label><br />
      		<label>
		       	Enter your email address:<br />
		        <input type="text" name="fieldemail" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>"/>
      		</label><br />
      		<label>
		        Date of birth:<br />
		        <input type="text" name="fielddate" <?php if ($errors['data']) {print 'class="error"';} ?> value="<?php print $values['data']; ?>" />
      		</label><br />
      		<label>
				Paul:<br />
				<div <?php if ($errors['pol']) {print 'class="error"';} ?>>
      			<input type="radio" name="radiogroup1" <?php if ($errors['pol']and$values['pol']=="Male") {print 'checked="checked"';}?> value="Male" />
        		Male
        	</label>
			<label>
      			<input type="radio" name="radiogroup1" <?php if ($errors['pol']and$values['pol']=="Female") {print 'checked="checked"';}?> value="Female" />
        		Female</label><br />
			<label>
				<input type="radio" name="radiogroup1" <?php if ($errors['pol']and$values['pol']=="Another") {print 'checked="checked"';}?> value="Another"  />
        		Another...?
        	</label>
        	</div><br />
        	Number of limbs:<br />
        	<div <?php if ($errors['konch']) {print 'class="error"';} ?>>
      		<label>
      			<input type="radio" name="radiogroup2" <?php if ($errors['konch']and$values['konch']=="Even") {print 'checked="checked"';}?>  value = "Even"/>
      			Even
      		</label>
      		<label>
      			<input type="radio" name="radiogroup2" <?php if ($errors['konch']and$values['konch']=="Odd") {print 'checked="checked"';}?>  value = "Odd"/>
        		Odd
        	</label><br />
			<label>
				<input type="radio" name="radiogroup2" <?php if ($errors['konch']and$values['konch']=="Another") {print 'checked="checked"';}?> value = "Another" />
        		Another...?
        	</label><br />
        	</div>
        	<label>
				Superpowers:<br />
        		<select name="fieldultimative[]" multiple="multiple" <?php if ($errors['sverh']) {print 'class="error"';} ?>>
			          <option <?php if ($errors['sverh']and$values['sverh']=="I eat and don't get fat") {print 'selected="selected"';}?>>I eat and don't get fat</option>
			          <option <?php if ($errors['sverh']and$values['sverh']=="I do not sleep and remain functional") {print 'selected="selected"';}?>>I do not sleep and remain functional</option>
			          <option <?php if ($errors['sverh']and$values['sverh']=="There is no fear of being expelled(immortality)") {print 'selected="selected"';}?>>There is no fear of being expelled(immortality)</option>
					  <option <?php if ($errors['sverh']and$values['sverh']=="I can program in Python") {print 'selected="selected"';}?>>I can program in Python</option>
					  <option <?php if ($errors['sverh']and$values['sverh']=="I deliver projects on time") {print 'selected="selected"';}?>>I deliver projects on time</option>
					  <option <?php if ($errors['sverh']and$values['sverh']=="I understand the joke") {print 'selected="selected"';}?>>I understand the joke</option>
					  <option <?php if ($errors['sverh']and$values['sverh']=="Absent") {print 'selected="selected"';}?>>Absent</option>
        		</select>
      		</label><br/>
			<label>
				Biography:<br/>
				<textarea name="bio"  <?php if ($errors['bio']) {print 'class="error"';} ?>><?php print $values['bio']; ?></textarea>
			</label><br />
			<label>
				<input type="checkbox" name="check1" />`
				The form above is filled in by me
			</label><br/>
			<input type="submit" value="A button to click on"/>
		</form>
	</div>
</body>
</html>